<?php

use Illuminate\Database\Seeder;

class AnggotasTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    \App\Anggota::insert([
      [
        'id'        => 1,
        'user_id'      => 1,
        'npm'        => 00000001,
        'nama'       => 'admin',
        'tempat_lahir'  => 'Malang',
        'tgl_lahir'    => '2000-01-01',
        'jk'        => 'L',
        'prodi'      => 'TI',
        'created_at'      => \Carbon\Carbon::now(),
        'updated_at'      => \Carbon\Carbon::now()
      ],
      [
        'id'        => 2,
        'user_id'      => 2,
        'npm'        => 18650003,
        'nama'       => 'Aditya',
        'tempat_lahir'  => 'Sidoarjo',
        'tgl_lahir'    => '2000-03-14',
        'jk'        => 'L',
        'prodi'      => 'TI',
        'created_at'      => \Carbon\Carbon::now(),
        'updated_at'      => \Carbon\Carbon::now()
      ],
    ]);
  }
}
