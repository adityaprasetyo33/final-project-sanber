<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    \App\User::insert([
      [
        'id'        => 1,
        'name'        => 'admin',
        'username'    => 'admin',
        'email'       => 'admin@admin.com',
        'password'    => bcrypt('admin'),
        'gambar'      => NULL,
        'level'      => 'admin',
        'remember_token'  => NULL,
        'created_at'      => \Carbon\Carbon::now(),
        'updated_at'      => \Carbon\Carbon::now()
      ],
      [
        'id'        => 2,
        'name'        => 'Aditya',
        'username'    => 'adit',
        'email'       => 'adit@user.com',
        'password'    => bcrypt('adit'),
        'gambar'      => NULL,
        'level'      => 'user',
        'remember_token'  => NULL,
        'created_at'      => \Carbon\Carbon::now(),
        'updated_at'      => \Carbon\Carbon::now()
      ]
    ]);
  }
}
